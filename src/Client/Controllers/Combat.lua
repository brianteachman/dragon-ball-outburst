-- Controller: Combat
--
-- By DadMonger and PantherStyle101, 2020


local Combat = {hitCount = 0}

-- Combat.hitCount = 0

-- The amount to increase the xp every strike.
local XP_BUMP = 20

-- The amount to decrease the other player's Health
local HP_DAMAGE = 15


function Combat:Start()

    ---------------------------------------------------------------------------
    -- Game/AGF Services, Controllers and Modules used in this Controller
    ---------------------------------------------------------------------------

    local Workspace = game:GetService("Workspace")

    local ReplicatedStorage = game:GetService("ReplicatedStorage")

    -- Get the AGF Keyboard Controller
    local keyboard = self.Controllers.UserInput:Get("Keyboard")

    ---------------------------------------------------------------------------
    -- 
    ---------------------------------------------------------------------------

    -- A training dummy requires a Humanoid and BoolValue called IsDamagable
    -- as children of the root element.
    local dummy = Workspace:WaitForChild("TrainingDummy")

    ---------------------------------------------------------------------------
    -- Animations
    ---------------------------------------------------------------------------

    -- Create a new animation instance.
    local punch = Instance.new("Animation")
    punch.AnimationId = "http://www.roblox.com/asset/?id=4560202169"

    ---------------------------------------------------------------------------
    -- Events
    ---------------------------------------------------------------------------

    -- Runs when players character is added to game
	self.Player.CharacterAdded:Connect(function(char)
		print("Player " .. self.Player.Name .. " added.")
        
        -- Must be true to be able to PVP.
        local isDamagable = Instance.new("BoolValue")
        isDamagable.Name = "IsDamagable"
        isDamagable.Value = true
		isDamagable.Parent = char
    end)
    
    -- Setup the 'Strike' event:
    self:ConnectEvent("Strike", function(otherPlayer)
        print(self.Player.Name .. " hit " .. otherPlayer.Name)

        -- Award experience points, increase players power level (XP)
        self.Shared.PowerLevelManager.updateLevel(self.Player, XP_BUMP)
        
        -- If you hit another player, decrease their hit points (HP).
        if (otherPlayer.Name ~= "TrainingDummy") then
            self.Modules.HealthManager.applyDamage(otherPlayer, HP_DAMAGE)
        end

        -- Update the scoreboard
        self.hitCount = self.hitCount + 1
        self.Controllers.Round:UpdateDisplay()

        -- TODO: Alert server to broadcast new player stats.

    end)

    ---------------------------------------------------------------------------
    -- Runtime
    ---------------------------------------------------------------------------

    repeat wait() until self.Player and self.Player.Character

    local character = self.Player.Character
    character:WaitForChild("Humanoid")

    local db = true -- Debounce variable. This looks like it should be false
    local damaged = false

    -- -- Function to call when the players right hand is touched
    -- -- partHit is the part that you hit, possibly another player
    character:WaitForChild("RightHand").Touched:Connect( function(partHit)
        
        -- Your player touched something with his hand, so make a sound.
        self.Shared.AudioPlayer.playAudio("Punch")

        -- If right hand hit a player
        -- 	And debounce (db) is false
        -- 	And damaged is false
        -- 	And we didnt hit ourself
        if partHit.Parent:FindFirstChild("Humanoid") and not db and not damaged and partHit.Parent.Humanoid ~= character.Humanoid then
            
            -- If players health is greater than 0
            if self.Player.Character.Humanoid.Health > 0 then
                damaged = true
                
                -- Notify other player that they have been hit using RemoteEvent class
                self:FireEvent("Strike", partHit.Parent)
                
            end
        end
    end)

    -- If the player presses the "E" key then load the animation.
    keyboard.KeyDown:Connect( function(keyCode)
        
        -- Punch Animation
        if keyCode == Enum.KeyCode.E and db then
            print(self.Player.Name .. " Punched [E].")
            db = false
            local anim = character.Humanoid:LoadAnimation(punch)
            anim:Play()
            wait(0.8)
            db = true
            damaged = false
        end
    end)

end

function Combat:Init()
    self:RegisterEvent("Strike")

    self.Shared.AudioPlayer.preloadAudio({
        ["Punch"] = 2174935713
    })
end

return Combat