--- Controller: Round
--
-- By DadMonger and PantherStyle101, 2020

local Round = {
    timerLabel = nil,
    hitCountLabel = nil,
    timeStarted = 0,
    timeRemaining = 0
}

---------------------------------------------------------------------------
-- UI Methods
---------------------------------------------------------------------------

-- Countdown Timer (time left in round)
function Round:UpdateTimer()
    -- 
    local duration = (tick() - self.timeStarted)

    -- The amount of time left in seconds (cant be negative).
    local timeLeft = math.clamp((self.timeRemaining - duration), 0, math.huge)

    -- Set SurfaceGui timer label number of seconds left in the round.
    self.timerLabel.Text = math.floor(timeLeft)
end

-- Manage text output to the StarterGui.ScreenGui.TextLable object.
function Round:UpdateDisplay(winner)
    --
    local hitCount = self.Controllers.Combat.hitCount -- TODO: Fix this hard dependancy
    self.hitCountLabel.Text = winner and (winner.Name .. " wins!") or ("Strikes: " .. hitCount)
end

---------------------------------------------------------------------------
-- Interface Methods
---------------------------------------------------------------------------

function Round:Start()

    ---------------------------------------------------------------------------
    -- Game/AGF Services, Controllers and Modules used in this Controller
    ---------------------------------------------------------------------------

    local Workspace = game:GetService("Workspace")

    -- This is our Server/Services/RoundService object.
    local roundService = self.Services.RoundService

    ---------------------------------------------------------------------------
    -- UI Components
    ---------------------------------------------------------------------------

    -- Scoreboard: Top-level Part containing a SurfaceGui for displaying ingame data.
    local scoreboard = Workspace:WaitForChild("Scoreboard"):WaitForChild("SurfaceGui")
    self.timerLabel = scoreboard:WaitForChild("Timer")       -- TextLabel
    self.hitCountLabel = scoreboard:WaitForChild("HitCount") -- TextLabel

    ---------------------------------------------------------------------------
    -- Events
    ---------------------------------------------------------------------------
    
    -- When RoundStarted event is called on the server, do this:
    roundService.RoundStarted:Connect( function(roundTime)
        -- 
        self.timeRemaining = roundTime
        self.timeStarted = tick()
        self:UpdateDisplay()
    end)

    -- When RoundFinished event is called with player.Name on the server, do this:
    roundService.RoundFinished:Connect( function(winner)
        -- 
        self:UpdateDisplay(winner)
    end)

    ---------------------------------------------------------------------------
    -- Start Round
    ---------------------------------------------------------------------------

    -- Update display when this module is started (first time).
    self:UpdateDisplay()

    -- If player joins after round has started update timer accordingly.
    local tr = roundService:GetRoundTimeRemaining()
    if (tr) then
        self.timeRemaining = tr
        self.timeStarted = tick()
        self:UpdateTimer()
    end

    -- Every millisecond update timer.
    while (true) do

        wait(0.1)   -- TODO: Should this be a blocking operation?
        self:UpdateTimer()
    end

end


function Round:Init()
	
end


return Round