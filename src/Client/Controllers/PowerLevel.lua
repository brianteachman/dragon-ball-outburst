-- Controller: Power Level
--
-- By DadMonger and PantherStyle101, 2020


local PowerLevel = {}


function PowerLevel:Init()

    ---------------------------------------------------------------------------
    -- Register Events
    ---------------------------------------------------------------------------

    self:RegisterEvent("SuppressPower")

    ---------------------------------------------------------------------------
    -- Add Player to the PowerLevelManager
    ---------------------------------------------------------------------------

    self.Shared.PowerLevelManager.initialize(self.Player)
    
    ---------------------------------------------------------------------------
    -- Setup Leaderboard: Display player's power level and current form
    ---------------------------------------------------------------------------

    -- Must be a Folder named "leaderstats" having the Player as a Parent.
    local leaderstats = Instance.new("Folder")
    leaderstats.Name = "leaderstats"
    leaderstats.Parent = self.Player
    
    -- As a child of "leaderstats," PowerLevel will shown in the Leaderboard.
    local levelLabel = Instance.new("IntValue")
    levelLabel.Name = "PowerLevel"
    levelLabel.Parent = leaderstats
    levelLabel.Value = self.Shared.PowerLevelManager.getPowerLevel(self.Player)
    
    -- As a child of "leaderstats," max Form will shown in the Leaderboard.
    local formLabel = Instance.new("StringValue")
    formLabel.Name = "Form"
    formLabel.Parent = leaderstats
    formLabel.Value = self.Shared.PowerLevelManager.getForm(self.Player)

end


function PowerLevel:Start()

    ---------------------------------------------------------------------------
    -- Game/AGF Services, Controllers and Modules used in this Controller
    ---------------------------------------------------------------------------

    -- Get the AGF Keyboard Controller
    local keyboard = self.Controllers.UserInput:Get("Keyboard")

    ---------------------------------------------------------------------------
    -- UI Components
    ---------------------------------------------------------------------------

    -- Top-level ScreenGUI holding our UI elements.
    local playerGui = game.Players.LocalPlayer:WaitForChild("PlayerGui") -- StarterGui
    -- local playerGui = game:GetService("StarterGui")

    -- SupressionMenu: Menu for suppressing player's power level
    local suppressionMenu = playerGui:WaitForChild("SupressionMenu") -- ScreenGui
    local frame = suppressionMenu:WaitForChild("Frame")
    local saveButton = frame:WaitForChild("SaveButton")
    local textBox = frame:WaitForChild("TextBox")
    
    ---------------------------------------------------------------------------
    -- UI Model
    ---------------------------------------------------------------------------

    -- Is GUI window displayed on screen?
    local toggled = true

    function ToggleWindow()
        toggled = not toggled
        suppressionMenu.Enabled = toggled
    end

    ---------------------------------------------------------------------------
    -- Connect Events
    ---------------------------------------------------------------------------

    -- Detect when player presses the "L" key.
    keyboard.KeyDown:Connect( function(keyCode)
        -- 
        if keyCode == Enum.KeyCode.L then

            -- Do the thing
            ToggleWindow()
        end
    end)

    -- Setup the 'SuppressPower' event:
    self:ConnectEvent("SuppressPower", function(factor)
        -- TODO: Suppress power level output on the Leaderboard.
        self.Shared.PowerLevelManager.setSuppressFactor(self.Player, factor)
        self.Shared.PowerLevelManager.suppressLevel(self.Player, true)

        print(self.Player.Name .. " supressed their power level to " .. factor)
    end)

    saveButton.Activated:Connect( function()
        local factor = tonumber(textBox.Text)
        if factor ~= nil then
            --
            -- Fire the 'SuppressPower' event:
            self:FireEvent("SuppressPower", factor)
        end
        
        ToggleWindow()
    end)

    ---------------------------------------------------------------------------
    -- Start Runtime
    ---------------------------------------------------------------------------

    ToggleWindow()

    -- TESTING ----------------------------------------------------------------
    self.Shared.PowerLevelManager.updateLevel(self.Player, 500)
    -- TESTING ----------------------------------------------------------------

end


return PowerLevel