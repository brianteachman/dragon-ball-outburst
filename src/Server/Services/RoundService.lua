-- Service: RoundService
--
-- By DadMonger and PantherStyle101, 2020



local RoundService = {Client = {}}

local ROUND_TIME_LIMIT = 30

local isRoundPlaying = false

local startedTime

--
function RoundService:PlayerStats(obj)
    -- Keep track of the number points awarded during match.
    local track = {}
        
    -- If the part was touched by a player, increase hit count and alert client.
    obj.Touched:Connect(function(partHit)
        local player = game.Players:GetPlayerFromCharacter(partHit.Parent)
        if (player) then
            if (not track[player]) then
                track[player] = 0
            end
            track[player] = track[player] + 1
            -- self:FireClient("PointSuccess", player, track[player])
        end
    end)

    return track
end

function RoundService:DoRound()

    isRoundPlaying = true
    startedTime = tick()

    local dummy = game:GetService("Workspace"):WaitForChild("TrainingDummy")

    -- A Table of player stats for this round.
    local track = self:PlayerStats(dummy. torso)

    -- Start the current round, passing round time so we can display remaining time.
    self:FireAllClients("RoundStarted", ROUND_TIME_LIMIT)

    -- Wait for the round to end.
    wait(ROUND_TIME_LIMIT)

    -- Determine the who the winner is.
    local winner
    local mostPoints = -1
    for player, count in pairs(track) do
        if (count > mostPoints) then
            mostPoints = count
            winner = player
        end
    end

    -- Round finished, pass winning player to the Event.
    self:FireAllClients("RoundFinished", winner)

    -- Turn off isRoundPlaying flag.
    isRoundPlaying = false
end

function RoundService.Client:GetRoundTimeRemaining()
    if (isRoundPlaying) then
        --
        local now = tick()
        local dur = now - startedTime
        local timeRemaining = ROUND_TIME_LIMIT - dur
        return timeRemaining
    end
end

-- ----------------------------------------------------------------------------
-- The Main game loop.
-- ----------------------------------------------------------------------------

function RoundService:Start()

    -- The Main Game Loop
    while (true) do
        --
        self:DoRound()

        -- TODO: Check if any player accepts another players challenge.
        wait(5) -- Intermission (time between rounds)
    end
	
end

-- ----------------------------------------------------------------------------
-- Initialize events and other setup required at runtime.
-- ----------------------------------------------------------------------------

function RoundService:Init()

    -- Register events to 'hook' into the game loop.
    self:RegisterClientEvent("RoundStarted")
    self:RegisterClientEvent("RoundFinished")
end

-- ----------------------------------------------------------------------------

return RoundService