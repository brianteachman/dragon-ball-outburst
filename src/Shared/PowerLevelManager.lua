--- Module: Power Level Manager
--
-- By DadMonger and PantherStyle101, 2020p


-- Private table that holds players form state
local playerdata = {}

-- Used to suppress (scale down, hide) the players power level
local DEFUALT_SUPPRESSION_FACTOR = 0.55

-- 
local PowerLevelManager = {}

--------------------------------------------------------------------------
-- Public module functions
--------------------------------------------------------------------------

function PowerLevelManager.initialize(player)
	if not playerdata[player.UserId] then
		
		playerdata[player.UserId] = {
			PowerLevel = 0, 
			Form = "Base", 
			Command = "base",
			IsSuppressed = false,
			SuppressionFactor = DEFUALT_SUPPRESSION_FACTOR
		}
		print("Created entry for player " .. player.UserId .. ".")
	end
end

function PowerLevelManager.updateLevel(player, levelBump)
	local uid = player.UserId
	--
	playerdata[uid].PowerLevel = playerdata[uid].PowerLevel + levelBump
	
	UpdateForm(player)
	
	-- Update the players playerdata[player.UserId] PowerLevel
	SetLeaderStats(player)
end 

function PowerLevelManager.suppressLevel(player, isSuppressed)
	playerdata[player.UserId].IsSuppressed = isSuppressed
	SetLeaderStats(player)
end

function PowerLevelManager.setSuppressFactor(player, factor)
	factor = NormalizeFactor(factor)
	playerdata[player.UserId].SuppressionFactor = factor
end

function PowerLevelManager.getPowerLevel(player)
	--
	if playerdata[player.UserId] then
		return playerdata[player.UserId].PowerLevel
	end
end

function PowerLevelManager.getForm(player)
	--
	return playerdata[player.UserId].Form
end

--------------------------------------------------------------------------
-- Private local functions
--------------------------------------------------------------------------

-- Recursive function that takes any number and returns 
-- a number greater than 0 and less than or equal to 1.
function NormalizeFactor(factor)
	if factor > 100 then
		--
		return NormalizeFactor(factor - 100)
	elseif factor > 1 or factor <= 100 then
		--
		return factor / 100
	elseif factor > 0 or factor <= 1 then
		--
		return factor
	else
		return 1
	end
end

function SetLeaderStats(player)
	local leaderstats = player:FindFirstChild("leaderstats")
	
	local powerlevel = playerdata[player.UserId].PowerLevel
	if playerdata[player.UserId].IsSuppressed then
		powerlevel = powerlevel * playerdata[player.UserId].SuppressionFactor
	end
	leaderstats:WaitForChild("PowerLevel").Value = powerlevel
	
	--leaderstats:WaitForChild("Form").Value = playerdata[player.UserId].Form
	leaderstats:WaitForChild("Form").Value = playerdata[player.UserId].Command
end

function UpdateForm(player)
	--
	local powerlevel = playerdata[player.UserId].PowerLevel
	if powerlevel >= 3500 then
		--
		playerdata[player.UserId].Form = "Master Ultra Instinct"		-- mui
		playerdata[player.UserId].Command = "mui"
		--
	elseif powerlevel >= 3000 then
		--
		playerdata[player.UserId].Form = "Ultra Instinct"		-- ui
		playerdata[player.UserId].Command = "ui"
		--
	elseif powerlevel >= 2200 then
		--
		playerdata[player.UserId].Form = "Super Saiyan Rose"  -- ssr
		playerdata[player.UserId].Command = "ssr"
		--
	elseif powerlevel >= 1500 then
		--
		playerdata[player.UserId].Form = "Super Saiyan Blue"	-- ssb
		playerdata[player.UserId].Command = "ssb"
		--
	elseif powerlevel >= 1000 then
		--
		playerdata[player.UserId].Form = "Super Saiyan  God" 	-- ssg
		playerdata[player.UserId].Command = "ssg"
		--
	elseif powerlevel >= 750 then
		--
		playerdata[player.UserId].Form = "Mystic"				-- myst
		playerdata[player.UserId].Command = "myst"
		--
	elseif powerlevel >= 600 then
		--
		playerdata[player.UserId].Form = "Legendary Super Saiyan 2" -- lss2
		playerdata[player.UserId].Command = "lss2"
		--
	elseif powerlevel >= 300 then
		--
		playerdata[player.UserId].Form = "Super Saiyan 3"		-- ss3
		playerdata[player.UserId].Command = "ss3"
		--
	elseif powerlevel >= 250 then
		--
		playerdata[player.UserId].Form = "Legendary Super Saiyan" -- lss
		playerdata[player.UserId].Command = "lss"
		--
	elseif powerlevel >= 200 then
		--
		playerdata[player.UserId].Form = "Super Saiyan 2"		-- ss2
		playerdata[player.UserId].Command = "ss2"
		--
	elseif powerlevel >= 150 then
		--
		playerdata[player.UserId].Form = "Super Saiyan Grade 2"	-- ssg2
		playerdata[player.UserId].Command = "ssg2"
		--
	elseif powerlevel >= 100 then
		--
		playerdata[player.UserId].Form = "Super Saiyan"		-- ss
		playerdata[player.UserId].Command = "ss"
		--
	elseif powerlevel >= 75 then
		--
		playerdata[player.UserId].Form = "Kaioken X20"		-- kk20
		playerdata[player.UserId].Command = "kk20"
		--
	elseif powerlevel >= 60 then
		--
		playerdata[player.UserId].Form = "Kaioken X10"		-- kk10
		playerdata[player.UserId].Command = "kk10"
		--
	elseif powerlevel >= 30 then
		--
		playerdata[player.UserId].Form = "Kaioken X2"			-- kk2
		playerdata[player.UserId].Command = "kk2"
		--
	elseif powerlevel >= 15 then
		--
		playerdata[player.UserId].Form = "Kaioken"			-- kk
		playerdata[player.UserId].Command = "kk"
	end
end


return PowerLevelManager
